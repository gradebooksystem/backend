package ru.omsu.imit.gradebooksystem.models

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name = "\"group\"")
data class Group(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long?,
        var name: String,

        @JsonIgnore
        @OneToMany(mappedBy = "group", fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
        var recordBooks: MutableList<RecordBook>?,

        @JsonIgnore
        @OneToMany(mappedBy = "group", fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
        var gradeBooks: MutableList<GradeBook>?
)