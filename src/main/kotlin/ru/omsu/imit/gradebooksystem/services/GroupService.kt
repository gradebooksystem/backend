package ru.omsu.imit.gradebooksystem.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.omsu.imit.gradebooksystem.models.Group
import ru.omsu.imit.gradebooksystem.repositories.GroupRepository

@Service
class GroupService {

    @Autowired
    lateinit var groupRepository: GroupRepository

    fun getAll(): List<Group> {
        return groupRepository.findAll()
    }

    fun getById(id: Long): Group {
        return groupRepository.getOne(id)
    }

    fun add(group: Group): Group {
        return groupRepository.save(group)
    }

    fun edit(id: Long, group: Group): Group {
        group.id = id
        return groupRepository.save(group)
    }

    fun deleteById(id: Long) {
        groupRepository.deleteById(id)
    }
}