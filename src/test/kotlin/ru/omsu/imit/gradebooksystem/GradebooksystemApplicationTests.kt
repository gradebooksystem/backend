package ru.omsu.imit.gradebooksystem

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.test.context.junit.jupiter.SpringExtension
import ru.omsu.imit.gradebooksystem.models.Group
import ru.omsu.imit.gradebooksystem.models.Subject
import ru.omsu.imit.gradebooksystem.models.Teacher
import ru.omsu.imit.gradebooksystem.repositories.*


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension::class)
@AutoConfigureTestDatabase
class GradebooksystemApplicationTests {

    @Autowired
    lateinit var testRestTemplate: TestRestTemplate

    @Autowired
    lateinit var groupRepository: GroupRepository

    @Autowired
    lateinit var recordBookRepository: RecordBookRepository

    @Autowired
    lateinit var recordBookEntryRepository: RecordBookEntryRepository

    @Autowired
    lateinit var subjectRepository: SubjectRepository

    @Autowired
    lateinit var teacherRepository: TeacherRepository

    @BeforeEach
    fun cleanDb() {
        groupRepository.deleteAllInBatch()
        recordBookRepository.deleteAllInBatch()
        recordBookEntryRepository.deleteAllInBatch()
        subjectRepository.deleteAllInBatch()
        teacherRepository.deleteAllInBatch()
    }

    @Test
    fun contextLoads() {
    }

    @Test
    fun insertGetAndDeleteTwoGroups() {
        val group1 = Group(null, "Test Group", null, null)
        val group2 = Group(null, "Тестовая Группа", null, null)
        val response1 = testRestTemplate.postForEntity("/api/groups", group1, Group::class.java)
        val response2 = testRestTemplate.postForEntity("/api/groups", group2, Group::class.java)
        assertEquals(200, response1.statusCodeValue)
        assertEquals(200, response2.statusCodeValue)
        val body1 = response1.body
        val body2 = response2.body
        assertTrue(body1?.id!! > 0)
        assertTrue(body2?.id!! > 0)
        group1.id = body1.id
        group2.id = body2.id
        assertEquals(group1, body1)
        assertEquals(group2, body2)

        val response3 = testRestTemplate.getForObject("/api/groups", Array<Group>::class.java).toList()
        assertEquals(2, response3.size)
        assertEquals(group1, response3[0])
        assertEquals(group2, response3[1])
        val response4 = testRestTemplate.getForObject("/api/groups/${group1.id}", Group::class.java)
        val response5 = testRestTemplate.getForObject("/api/groups/${group2.id}", Group::class.java)
        assertEquals(group1, response4)
        assertEquals(group2, response5)

        val response6 = testRestTemplate.exchange("/api/groups/${group1.id}", HttpMethod.DELETE, HttpEntity(""), String::class.java)
        val response7 = testRestTemplate.exchange("/api/groups/${group2.id}", HttpMethod.DELETE, HttpEntity(""), String::class.java)
        assertEquals(200, response6.statusCodeValue)
        assertEquals(200, response7.statusCodeValue)
        val response8 = testRestTemplate.exchange("/api/groups/${group2.id}", HttpMethod.DELETE, HttpEntity(""), String::class.java)
        assertEquals(400, response8.statusCodeValue)

        val response9 = testRestTemplate.getForEntity("/api/groups/${group1.id}", String::class.java)
        assertEquals(400, response9.statusCodeValue)
        val response10 = testRestTemplate.getForObject("/api/groups", Array<Group>::class.java).toList()
        assertEquals(0, response10.size)
    }

    @Test
    fun editGroup() {
        val group1 = Group(null, "Test Group", null, null)
        val group2 = Group(null, "Тестовая Группа", null, null)
        val response1 = testRestTemplate.postForEntity("/api/groups", group1, Group::class.java)
        assertEquals(group1.name, response1.body?.name)
        val response2 = testRestTemplate.exchange("/api/groups/${response1.body?.id}", HttpMethod.PUT, HttpEntity(group2), Group::class.java)
        assertEquals(200, response2.statusCodeValue)
        assertEquals(response1.body?.id, response2.body?.id)
        assertEquals(group2.name, response2.body?.name)
    }

    @Test
    fun insertGetAndDeleteTwoTeachers() {
        val teacher1 = Teacher(null, "Test Teacher", null, null)
        val teacher2 = Teacher(null, "Тестовый Преподаватель", null, null)
        val response1 = testRestTemplate.postForEntity("/api/teachers", teacher1, Teacher::class.java)
        val response2 = testRestTemplate.postForEntity("/api/teachers", teacher2, Teacher::class.java)
        assertEquals(200, response1.statusCodeValue)
        assertEquals(200, response2.statusCodeValue)
        val body1 = response1.body
        val body2 = response2.body
        assertTrue(body1?.id!! > 0)
        assertTrue(body2?.id!! > 0)
        teacher1.id = body1.id
        teacher2.id = body2.id
        assertEquals(teacher1, body1)
        assertEquals(teacher2, body2)

        val response3 = testRestTemplate.getForObject("/api/teachers", Array<Teacher>::class.java).toList()
        assertEquals(2, response3.size)
        assertEquals(teacher1, response3[0])
        assertEquals(teacher2, response3[1])
        val response4 = testRestTemplate.getForObject("/api/teachers/${teacher1.id}", Teacher::class.java)
        val response5 = testRestTemplate.getForObject("/api/teachers/${teacher2.id}", Teacher::class.java)
        assertEquals(teacher1, response4)
        assertEquals(teacher2, response5)

        val response6 = testRestTemplate.exchange("/api/teachers/${teacher1.id}", HttpMethod.DELETE, HttpEntity(""), String::class.java)
        val response7 = testRestTemplate.exchange("/api/teachers/${teacher2.id}", HttpMethod.DELETE, HttpEntity(""), String::class.java)
        assertEquals(200, response6.statusCodeValue)
        assertEquals(200, response7.statusCodeValue)
        val response8 = testRestTemplate.exchange("/api/teachers/${teacher2.id}", HttpMethod.DELETE, HttpEntity(""), String::class.java)
        assertEquals(400, response8.statusCodeValue)

        val response9 = testRestTemplate.getForEntity("/api/teachers/${teacher1.id}", String::class.java)
        assertEquals(400, response9.statusCodeValue)
        val response10 = testRestTemplate.getForObject("/api/teachers", Array<Teacher>::class.java).toList()
        assertEquals(0, response10.size)
    }

    @Test
    fun editTeacher() {
        val teacher1 = Teacher(null, "Test Teacher", null, null)
        val teacher2 = Teacher(null, "Тестовый Преподаватель", null, null)
        val response1 = testRestTemplate.postForEntity("/api/teachers", teacher1, Teacher::class.java)
        assertEquals(teacher1.name, response1.body?.name)
        val response2 = testRestTemplate.exchange("/api/teachers/${response1.body?.id}", HttpMethod.PUT, HttpEntity(teacher2), Teacher::class.java)
        assertEquals(200, response2.statusCodeValue)
        assertEquals(response1.body?.id, response2.body?.id)
        assertEquals(teacher2.name, response2.body?.name)
    }

    @Test
    fun insertGetAndDeleteTwoSubjects() {
        val subject1 = Subject(null, "Test Subject", null, null)
        val subject2 = Subject(null, "Тестовый Предмет", null, null)
        val response1 = testRestTemplate.postForEntity("/api/subjects", subject1, Subject::class.java)
        val response2 = testRestTemplate.postForEntity("/api/subjects", subject2, Subject::class.java)
        assertEquals(200, response1.statusCodeValue)
        assertEquals(200, response2.statusCodeValue)
        val body1 = response1.body
        val body2 = response2.body
        assertTrue(body1?.id!! > 0)
        assertTrue(body2?.id!! > 0)
        subject1.id = body1.id
        subject2.id = body2.id
        assertEquals(subject1, body1)
        assertEquals(subject2, body2)

        val response3 = testRestTemplate.getForObject("/api/subjects", Array<Subject>::class.java).toList()
        assertEquals(2, response3.size)
        assertEquals(subject1, response3[0])
        assertEquals(subject2, response3[1])
        val response4 = testRestTemplate.getForObject("/api/subjects/${subject1.id}", Subject::class.java)
        val response5 = testRestTemplate.getForObject("/api/subjects/${subject2.id}", Subject::class.java)
        assertEquals(subject1, response4)
        assertEquals(subject2, response5)

        val response6 = testRestTemplate.exchange("/api/subjects/${subject1.id}", HttpMethod.DELETE, HttpEntity(""), String::class.java)
        val response7 = testRestTemplate.exchange("/api/subjects/${subject2.id}", HttpMethod.DELETE, HttpEntity(""), String::class.java)
        assertEquals(200, response6.statusCodeValue)
        assertEquals(200, response7.statusCodeValue)
        val response8 = testRestTemplate.exchange("/api/subjects/${subject2.id}", HttpMethod.DELETE, HttpEntity(""), String::class.java)
        assertEquals(400, response8.statusCodeValue)

        val response9 = testRestTemplate.getForEntity("/api/subjects/${subject1.id}", String::class.java)
        assertEquals(400, response9.statusCodeValue)
        val response10 = testRestTemplate.getForObject("/api/subjects", Array<Subject>::class.java).toList()
        assertEquals(0, response10.size)
    }

    @Test
    fun editSubject() {
        val subject1 = Subject(null, "Test Subject", null, null)
        val subject2 = Subject(null, "Тестовый Предмет", null, null)
        val response1 = testRestTemplate.postForEntity("/api/subjects", subject1, Subject::class.java)
        assertEquals(subject1.name, response1.body?.name)
        val response2 = testRestTemplate.exchange("/api/subjects/${response1.body?.id}", HttpMethod.PUT, HttpEntity(subject2), Subject::class.java)
        assertEquals(200, response2.statusCodeValue)
        assertEquals(response1.body?.id, response2.body?.id)
        assertEquals(subject2.name, response2.body?.name)
    }

}
