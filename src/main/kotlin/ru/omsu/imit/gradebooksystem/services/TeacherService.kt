package ru.omsu.imit.gradebooksystem.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.omsu.imit.gradebooksystem.models.Teacher
import ru.omsu.imit.gradebooksystem.repositories.TeacherRepository

@Service
class TeacherService(
        @Autowired
        val teacherRepository: TeacherRepository
) {

    fun getAll(): List<Teacher> {
        return teacherRepository.findAll()
    }

    fun getById(id: Long): Teacher {
        return teacherRepository.getOne(id)
    }

    fun add(teacher: Teacher): Teacher {
        return teacherRepository.save(teacher)
    }

    fun edit(id: Long, teacher: Teacher): Teacher {
        teacher.id = id
        return teacherRepository.save(teacher)
    }

    fun deleteById(id: Long) {
        teacherRepository.deleteById(id)
    }
}