package ru.omsu.imit.gradebooksystem.models

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonIdentityReference
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import javax.persistence.*

@Entity
data class GradeBook(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long?,
        var semester: Int,

        @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id")
        @JsonIdentityReference(alwaysAsId = true)
        @ManyToOne
        var group: Group?,

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "gradeBook", cascade = [CascadeType.ALL])
        var entries: MutableList<GradeBookEntry>?
)