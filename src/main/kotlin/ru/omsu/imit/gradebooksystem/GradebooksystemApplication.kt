package ru.omsu.imit.gradebooksystem

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GradebooksystemApplication

fun main(args: Array<String>) {
    runApplication<GradebooksystemApplication>(*args)
}
