package ru.omsu.imit.gradebooksystem.exceptions

class BadRequestException(message: String) : Exception(message) {
    constructor() : this("Bad request.")
}