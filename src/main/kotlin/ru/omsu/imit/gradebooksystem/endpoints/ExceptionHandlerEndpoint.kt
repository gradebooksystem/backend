package ru.omsu.imit.gradebooksystem.endpoints

import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import ru.omsu.imit.gradebooksystem.exceptions.BadRequestException
import javax.persistence.EntityNotFoundException

@ControllerAdvice
class ExceptionHandlerEndpoint : ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [(BadRequestException::class)])
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    fun handleBadRequest(e: BadRequestException): String {
        return e.message.toString()
    }

    @ExceptionHandler(value = [(EmptyResultDataAccessException::class)])
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    fun handleEmptyResultDataAccessException(e: EmptyResultDataAccessException): String {
        return e.message.toString()
    }

    @ExceptionHandler(value = [(EntityNotFoundException::class)])
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    fun handleEmptyResultDataAccessException(e: EntityNotFoundException): String {
        return e.message.toString()
    }

    @ExceptionHandler(value = [(org.hibernate.exception.ConstraintViolationException::class)])
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    fun handleEmptyResultDataAccessException(e: org.hibernate.exception.ConstraintViolationException): String {
        return e.cause?.message.toString()
    }

}