package ru.omsu.imit.gradebooksystem.endpoints

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import ru.omsu.imit.gradebooksystem.models.Group
import ru.omsu.imit.gradebooksystem.services.GroupService

@RestController
@CrossOrigin(origins = ["http://localhost:3000"])
class GroupEndpoint {

    @Autowired
    lateinit var groupService: GroupService

    @GetMapping(path = ["/api/groups"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getAll(): List<Group> {
        return groupService.getAll()
    }

    @GetMapping(path = ["/api/groups/{id}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getById(@PathVariable id: Long): Group {
        return groupService.getById(id)
    }

    @PostMapping(path = ["/api/groups"], consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun add(@RequestBody group: Group): Group {
        return groupService.add(group)
    }

    @PutMapping(path = ["/api/groups/{id}"], consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun edit(@RequestBody group: Group, @PathVariable id: Long): Group {
        return groupService.edit(id, group)
    }

    @DeleteMapping(path = ["/api/groups/{id}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun deleteById(@PathVariable id: Long) {
        groupService.deleteById(id)
    }
}