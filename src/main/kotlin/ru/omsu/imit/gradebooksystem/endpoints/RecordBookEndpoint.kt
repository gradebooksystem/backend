package ru.omsu.imit.gradebooksystem.endpoints

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import ru.omsu.imit.gradebooksystem.models.RecordBook
import ru.omsu.imit.gradebooksystem.services.RecordBookService

@RestController
@CrossOrigin(origins = ["http://localhost:3000"])
class RecordBookEndpoint {

    @Autowired
    lateinit var recordBookService: RecordBookService

    @GetMapping(path = ["/api/record-books"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getAll(): List<ObjectNode> {
        val recordBooks = recordBookService.getAll()
        val result = recordBooks.map { rb -> jacksonObjectMapper().valueToTree<JsonNode>(rb) as ObjectNode }
        result.forEach { m -> m.remove("entries") }
        return result
    }

    @GetMapping(path = ["/api/record-books/{id}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getById(@PathVariable id: Long): RecordBook {
        return recordBookService.getById(id)
    }

    @PutMapping(path = ["/api/record-books/{id}"], consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun edit(@PathVariable id: Long, @RequestBody recordBook: RecordBook): RecordBook {
        return recordBookService.edit(id, recordBook)
    }
}