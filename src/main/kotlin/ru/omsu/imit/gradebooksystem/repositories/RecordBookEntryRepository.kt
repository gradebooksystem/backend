package ru.omsu.imit.gradebooksystem.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.omsu.imit.gradebooksystem.models.RecordBookEntry

@Repository
interface RecordBookEntryRepository : JpaRepository<RecordBookEntry, Long> {
    fun deleteAllByRecordBookId(id: Long)
}