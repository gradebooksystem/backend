package ru.omsu.imit.gradebooksystem.endpoints

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import ru.omsu.imit.gradebooksystem.models.Subject
import ru.omsu.imit.gradebooksystem.services.SubjectService

@RestController
@CrossOrigin(origins = ["http://localhost:3000"])
class SubjectEndpoint {

    @Autowired
    lateinit var subjectService: SubjectService

    @GetMapping(path = ["/api/subjects"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getAll(): List<Subject> {
        return subjectService.getAll()
    }

    @GetMapping(path = ["/api/subjects/{id}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getById(@PathVariable id: Long): Subject {
        return subjectService.getById(id)
    }

    @PostMapping(path = ["/api/subjects"], consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun add(@RequestBody subject: Subject): Subject {
        return subjectService.add(subject)
    }

    @PutMapping(path = ["/api/subjects/{id}"], consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun edit(@RequestBody subject: Subject, @PathVariable id: Long): Subject {
        return subjectService.edit(id, subject)
    }

    @DeleteMapping(path = ["/api/subjects/{id}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun deleteById(@PathVariable id: Long) {
        subjectService.deleteById(id)
    }
}