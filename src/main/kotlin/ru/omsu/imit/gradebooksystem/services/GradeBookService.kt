package ru.omsu.imit.gradebooksystem.services

import com.fasterxml.jackson.databind.JsonNode
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.omsu.imit.gradebooksystem.models.*
import ru.omsu.imit.gradebooksystem.repositories.*

@Service
class GradeBookService {

    @Autowired
    lateinit var gradeBookRepository: GradeBookRepository

    @Autowired
    lateinit var gradeBookEntryRepository: GradeBookEntryRepository

    @Autowired
    lateinit var gradeRepository: GradeRepository

    @Autowired
    lateinit var groupRepository: GroupRepository

    @Autowired
    lateinit var subjectRepository: SubjectRepository

    @Autowired
    lateinit var teacherRepository: TeacherRepository

    @Autowired
    lateinit var recordBookRepository: RecordBookRepository

    @Transactional
    fun add(map: JsonNode): GradeBook? {
        val kiup = map.get("KIUP0100")
        val groupName = kiup.get("CF_ГРУППА").textValue().trim()
        val semesterLine = kiup.get("CF_GR_SEMESTR").textValue()
        var semester = semesterLine.get(5).toString().toInt() * 2
        if (semesterLine.indexOf("зимняя") > -1) {
            semester--
        }

        var group = groupRepository.findByName(groupName)
        if (group == null) {
            group = groupRepository.saveAndFlush(Group(null, groupName, null, null))
        }

        var gradeBook = gradeBookRepository.findBySemesterAndGroupId(semester, group.id!!)
        if (gradeBook == null) {
            gradeBook = gradeBookRepository.saveAndFlush(GradeBook(null, semester, group, null))
        }
        gradeBookEntryRepository.deleteAllByGradeBookId(gradeBook.id!!)

        val students = kiup.get("LIST_G_CROSS").get("G_CROSS").get("LIST_G_СТУДЕНТ").get("G_СТУДЕНТ")
        for (student in students) {
            val name = student.get("ФИО").textValue().replace("\\(.*\\)".toRegex(), "").trim()
            var recordBook = recordBookRepository.findByName(name)
            if (recordBook == null) {
                recordBook = recordBookRepository.saveAndFlush(RecordBook(null, name, "", group, null, null))
            }
            val gradeBookEntry = gradeBookEntryRepository.saveAndFlush(GradeBookEntry(null, gradeBook, recordBook, null))
            val gradeList = student.get("LIST_G_ДИСЦИПЛИНА").get("G_ДИСЦИПЛИНА")
            for (gradeMap in gradeList) {
                val subjectTeacher = gradeMap.get("ИМД_КОРОТКОЕ_ИМЯ").textValue()
                val subjectName = subjectTeacher.split("\n")[0].trim()
                val teacherName = subjectTeacher.split("\n")[1].trim()
                var subject = subjectRepository.findByName(subjectName)
                if (subject == null) {
                    subject = subjectRepository.saveAndFlush(Subject(null, subjectName, null, null))
                }
                var teacher = teacherRepository.findByName(teacherName)
                if (teacher == null) {
                    teacher = teacherRepository.saveAndFlush(Teacher(null, teacherName, null, null))
                }
                val gradeValMap = gradeMap.get("LIST_G_ОЦЕНКА").get("G_ОЦЕНКА")
                val gradeRawNode = gradeValMap.get("СПИСОК")
                var gradeRaw: String
                gradeRaw = if (gradeRawNode.isTextual) {
                    gradeRawNode.textValue()
                } else {
                    gradeRawNode.longValue().toString()
                }
                val gradeClean = gradeValMap.get("ЭКВИВАЛЕНТ").longValue().toString()

                gradeRepository.saveAndFlush(Grade(null, gradeBookEntry, subject, teacher, gradeRaw, gradeClean))
            }
        }
        return gradeBookRepository.getOne(gradeBook.id!!)
    }

    fun getAll(): List<GradeBook> {
        return gradeBookRepository.findAll()
    }

    fun get(id: Long): GradeBook {
        return gradeBookRepository.getOne(id)
    }

    fun getErrors(id: Long): Map<Long, String> {
        val res: MutableMap<Long, String> = mutableMapOf()
        val gradeBook = gradeBookRepository.getOne(id)
        for (gradeBookEntry in gradeBook.entries!!) {
            val recordBookEntries = gradeBookEntry.recordBook?.entries
            val grades = gradeBookEntry.grades
            for (grade in grades!!) {
                if (recordBookEntries == null
                        || recordBookEntries.none { rbe -> rbe.semester == gradeBook.semester && rbe.subject == grade.subject }) {
                    res[grade.id!!] = "NOT_IN_RECORD_BOOK"
                } else {
                    val rbe = recordBookEntries.filter { rbe -> rbe.semester == gradeBook.semester && rbe.subject == grade.subject }[0]
                    if (rbe.grade != grade.grade) {
                        res[grade.id!!] = "GRADE_MISMATCH"
                    }
                }
            }
        }
        return res
    }

}