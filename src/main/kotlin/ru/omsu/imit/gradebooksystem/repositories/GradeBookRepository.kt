package ru.omsu.imit.gradebooksystem.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.omsu.imit.gradebooksystem.models.GradeBook

@Repository
interface GradeBookRepository : JpaRepository<GradeBook, Long> {
    fun findBySemesterAndGroupId(semester: Int, groupId: Long): GradeBook?
}