package ru.omsu.imit.gradebooksystem.helpers

import ru.omsu.imit.gradebooksystem.helpers.xmlconverter.XmlToJsonConverter

class GradebookParser {

    companion object {

        fun parse(file: String): String {
            val xmlToJsonConverter = XmlToJsonConverter()
            return xmlToJsonConverter.convert(file) ?: ""
        }
    }
}