package ru.omsu.imit.gradebooksystem.models

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
data class Subject(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long?,
        var name: String,

        @JsonIgnore
        @OneToMany(mappedBy = "subject", fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
        var recordBooks: MutableList<RecordBookEntry>?,

        @JsonIgnore
        @OneToMany(mappedBy = "subject", fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
        var grades: MutableList<Grade>?
)