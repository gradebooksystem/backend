package ru.omsu.imit.gradebooksystem.endpoints

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import ru.omsu.imit.gradebooksystem.helpers.GradebookParser
import ru.omsu.imit.gradebooksystem.models.GradeBook
import ru.omsu.imit.gradebooksystem.services.GradeBookService
import java.nio.charset.Charset

@RestController
@CrossOrigin(origins = ["http://localhost:3000"])
class GradeBookEndpoint {

    @Autowired
    lateinit var gradeBookService: GradeBookService

    @PostMapping(path = ["/api/gradebooks"], consumes = [MediaType.APPLICATION_XML_VALUE])
    fun uploadGradeBook(@RequestBody bytes: ByteArray): GradeBook? {
        val string = GradebookParser.parse(String(bytes, Charset.forName("Windows-1251")))
        val node = ObjectMapper().readTree(string)
        return gradeBookService.add(node)
    }

    @GetMapping(path = ["/api/gradebooks"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getAll(): List<GradeBook> {
        return gradeBookService.getAll()
    }

    @GetMapping(path = ["/api/gradebooks/{id}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun get(@PathVariable id: Long): String {
        val gradeBook = gradeBookService.get(id)
        val errors = gradeBookService.getErrors(id)
        return "{\"gradebook\":${jacksonObjectMapper().writeValueAsString(gradeBook)}," +
                "\"errors\":${jacksonObjectMapper().writeValueAsString(errors)}}"
    }
}