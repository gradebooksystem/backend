package ru.omsu.imit.gradebooksystem.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.omsu.imit.gradebooksystem.models.Subject
import ru.omsu.imit.gradebooksystem.repositories.SubjectRepository

@Service
class SubjectService(
        @Autowired
        val subjectRepository: SubjectRepository
) {

    fun getAll(): List<Subject> {
        return subjectRepository.findAll()
    }

    fun getById(id: Long): Subject {
        return subjectRepository.getOne(id)
    }

    fun add(subject: Subject): Subject {
        return subjectRepository.save(subject)
    }

    fun edit(id: Long, subject: Subject): Subject {
        subject.id = id
        return subjectRepository.save(subject)
    }

    fun deleteById(id: Long) {
        subjectRepository.deleteById(id)
    }
}