package ru.omsu.imit.gradebooksystem.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.omsu.imit.gradebooksystem.models.Teacher

@Repository
interface TeacherRepository : JpaRepository<Teacher, Long> {
    fun findByName(name: String): Teacher?
}