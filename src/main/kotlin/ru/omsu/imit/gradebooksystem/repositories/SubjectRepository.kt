package ru.omsu.imit.gradebooksystem.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.omsu.imit.gradebooksystem.models.Subject

@Repository
interface SubjectRepository : JpaRepository<Subject, Long> {
    fun findByName(name: String): Subject?
}