package ru.omsu.imit.gradebooksystem.models

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonIdentityReference
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import javax.persistence.*

@Entity
data class GradeBookEntry(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long?,

        @JsonIgnore
        @ManyToOne
        var gradeBook: GradeBook?,

        @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id")
        @JsonIdentityReference(alwaysAsId = true)
        @ManyToOne
        var recordBook: RecordBook?,

        @OneToMany(mappedBy = "gradeBookEntry", fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
        var grades: MutableList<Grade>?

)