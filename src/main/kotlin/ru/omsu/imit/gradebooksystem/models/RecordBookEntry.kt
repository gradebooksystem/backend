package ru.omsu.imit.gradebooksystem.models

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIdentityReference
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(uniqueConstraints = [UniqueConstraint(columnNames = ["record_book_id", "semester", "subject_id"])])
data class RecordBookEntry(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long?,

        @JsonIgnore
        @ManyToOne(fetch = FetchType.LAZY)
        var recordBook: RecordBook?,

        var semester: Int,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "en_US")
        var date: Date,
        var grade: String,

        @ManyToOne
        @JsonIdentityReference(alwaysAsId = true)
        var subject: Subject?,
        @ManyToOne
        @JsonIdentityReference(alwaysAsId = true)
        var teacher: Teacher?
) {
    companion object {
        const val NOT_FOUND = "Record book entry not found."
        const val MISMATCH = "Mismatched record book and entry."
    }
}