package ru.omsu.imit.gradebooksystem.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.omsu.imit.gradebooksystem.models.Group

@Repository
interface GroupRepository : JpaRepository<Group, Long> {
    fun findByName(name: String): Group?
}