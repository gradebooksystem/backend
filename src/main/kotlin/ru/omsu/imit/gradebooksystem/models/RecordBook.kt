package ru.omsu.imit.gradebooksystem.models

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonIdentityReference
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import javax.persistence.*

@Entity
data class RecordBook(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long?,
        var name: String,
        var code: String,

        @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "name")
        @JsonIdentityReference(alwaysAsId = true)
        @ManyToOne
        var group: Group?,

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "recordBook", cascade = [CascadeType.ALL])
        var entries: MutableList<RecordBookEntry>?,

        @JsonIgnore
        @OneToMany(fetch = FetchType.LAZY, mappedBy = "recordBook", cascade = [CascadeType.ALL])
        var gradeBookEntries: MutableList<GradeBookEntry>?
) {
    fun toMap(): MutableMap<String, Any> {
        val objectMapper = ObjectMapper()
        return objectMapper.convertValue(this, object : TypeReference<MutableMap<String, Any>>() {})
    }
}