package ru.omsu.imit.gradebooksystem.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.omsu.imit.gradebooksystem.models.RecordBook

@Repository
interface RecordBookRepository : JpaRepository<RecordBook, Long> {
    fun findByName(name: String): RecordBook?
}