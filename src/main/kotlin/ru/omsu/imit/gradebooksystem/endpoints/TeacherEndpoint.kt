package ru.omsu.imit.gradebooksystem.endpoints

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import ru.omsu.imit.gradebooksystem.models.Teacher
import ru.omsu.imit.gradebooksystem.services.TeacherService

@RestController
@CrossOrigin(origins = ["http://localhost:3000"])
class TeacherEndpoint {

    @Autowired
    lateinit var teacherService: TeacherService

    @GetMapping(path = ["/api/teachers"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getAll(): List<Teacher> {
        return teacherService.getAll()
    }

    @GetMapping(path = ["/api/teachers/{id}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getById(@PathVariable id: Long): Teacher {
        return teacherService.getById(id)
    }

    @PostMapping(path = ["/api/teachers"], consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun add(@RequestBody teacher: Teacher): Teacher {
        return teacherService.add(teacher)
    }

    @PutMapping(path = ["/api/teachers/{id}"], consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun edit(@RequestBody teacher: Teacher, @PathVariable id: Long): Teacher {
        return teacherService.edit(id, teacher)
    }

    @DeleteMapping(path = ["/api/teachers/{id}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun deleteById(@PathVariable id: Long) {
        teacherService.deleteById(id)
    }
}