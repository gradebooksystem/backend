package ru.omsu.imit.gradebooksystem.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.omsu.imit.gradebooksystem.models.RecordBook
import ru.omsu.imit.gradebooksystem.repositories.GroupRepository
import ru.omsu.imit.gradebooksystem.repositories.RecordBookEntryRepository
import ru.omsu.imit.gradebooksystem.repositories.RecordBookRepository

@Service
class RecordBookService {

    @Autowired
    lateinit var recordBookRepository: RecordBookRepository
    @Autowired
    lateinit var recordBookEntryRepository: RecordBookEntryRepository
    @Autowired
    lateinit var groupRepository: GroupRepository

    fun getAll(): List<RecordBook> {
        return recordBookRepository.findAll()
    }

    fun getById(id: Long): RecordBook {
        return recordBookRepository.getOne(id)
    }

    fun add(recordBook: RecordBook, groupId: Long): RecordBook {
        val group = groupRepository.getOne(groupId)
        recordBook.group = group
        val result = recordBookRepository.save(recordBook)
        if (recordBook.entries != null) {
            for (entry in recordBook.entries!!) {
                entry.recordBook = recordBook
                recordBookEntryRepository.save(entry)
            }
        }
        return recordBookRepository.getOne(result.id!!)
    }

    @Transactional
    fun edit(id: Long, recordBook: RecordBook): RecordBook {
        recordBook.id = id
        recordBookEntryRepository.deleteAllByRecordBookId(id)
        recordBookEntryRepository.flush()
        val result = recordBookRepository.save(recordBook)
        if (result.entries != null) {
            for (entry in result.entries!!) {
                entry.recordBook = recordBook
                recordBookEntryRepository.save(entry)
            }
        }
        return recordBookRepository.findById(result.id!!).get()
    }

    fun deleteById(id: Long) {
        recordBookRepository.deleteById(id)
    }
}